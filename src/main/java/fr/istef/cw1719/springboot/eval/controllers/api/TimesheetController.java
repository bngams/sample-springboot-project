package fr.istef.cw1719.springboot.eval.controllers.api;

import fr.istef.cw1719.springboot.eval.entities.Task;
import fr.istef.cw1719.springboot.eval.entities.Timesheet;
import fr.istef.cw1719.springboot.eval.entities.TimesheetTask;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

@RestController
@RequestMapping("/api/v1/timesheet")
public class TimesheetController {

    @GetMapping("/project/{id}")
    public Timesheet getProjectTimesheet(@PathVariable Long id) {
        Timesheet ts = new Timesheet();
        ts.setBu(this.getRandomNumberInRange(0, 16));
        ts.setId((long) this.getRandomNumberInRange(1000, 2000));
        ts.setLastUpdate(new Date());
        // members
        ArrayList<Integer> members = new ArrayList<>();
        int nbMembers = getRandomNumberInRange(2, 8);
        for(int i = 0; i < nbMembers; i++) {
            members.add(getRandomNumberInRange(1, 100));
        }
        ts.setMembers(members);
        // tasks
        ArrayList<TimesheetTask> tasks = new ArrayList<>();
        int nbTask = getRandomNumberInRange(2, 8);
        for(int i = 0; i < nbTask; i++) {
            TimesheetTask t = new TimesheetTask(
                   "",
                   getRandomNumberInRange(25,60),
                   getRandomNumberInRange(2,35)
           );
            tasks.add(t);
        }
        ts.setTasks(tasks);
        return ts;
    }

    private int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
