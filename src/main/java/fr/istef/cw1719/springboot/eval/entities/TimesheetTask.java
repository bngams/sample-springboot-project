package fr.istef.cw1719.springboot.eval.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimesheetTask {
    private String num;
    private Integer hourCost;
    private Integer duration;
}
