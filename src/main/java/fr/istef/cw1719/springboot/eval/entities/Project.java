package fr.istef.cw1719.springboot.eval.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity(name="projects")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Length(min = 3, max = 20)
    private String num;
    @Length(min = 3, max = 255)
    private String description;
    @Length(min = 3, max = 100)
    private String mode;
    private Integer budget;
    private Date startDate;
    @ManyToOne
    @JoinColumn(name="manager_id", nullable=false)
    private User user;
}
