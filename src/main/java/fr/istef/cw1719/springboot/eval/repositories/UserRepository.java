package fr.istef.cw1719.springboot.eval.repositories;

import fr.istef.cw1719.springboot.eval.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
