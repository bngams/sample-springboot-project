package fr.istef.cw1719.springboot.eval.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Timesheet {
    private Long id;
    private ArrayList<Integer> members;
    private Integer bu;
    private Date lastUpdate;
    private ArrayList<TimesheetTask> tasks;
}
