package fr.istef.cw1719.springboot.eval.repositories;

import fr.istef.cw1719.springboot.eval.entities.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {

}
